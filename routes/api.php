<?php

use App\Http\Controllers\CustomersController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'csv'], function () {
    Route::get('/list', [CustomersController::class, 'getCustomersData']);
    Route::post('/upload', [CustomersController::class, 'uploadCustomersData']);
});
