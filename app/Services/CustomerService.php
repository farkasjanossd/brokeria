<?php

namespace App\Services;

use App\Jobs\ProcessCsv;
use App\Models\Customer;
use App\Requests\CustomersUploadRequest;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

class CustomerService
{
    /**
     * Return paginated customer data.
     *
     * @param Request $request
     *
     * @return LengthAwarePaginator
     */
    public static function getPaginatedCustomerData(Request $request): LengthAwarePaginator
    {
        $customerQuery = Customer::query();
        $querySearch = $request->get('q');
        if (!empty($querySearch)) {
            $customerQuery->whereAny(['user_name', 'email', 'credit_card_issuer', 'iban', 'ipv4', 'birth_date'], 'like', '%' . $querySearch . '%');
        }

        return $customerQuery
            ->paginate($request->get('limit', 20));
    }

    /**
     * Upload and save function.
     *
     * @param CustomersUploadRequest $request
     *
     * @return void
     */
    public static function uploadAndProcessFile(CustomersUploadRequest $request): void
    {
        //create new background job
        $csvFile = $request->validated('csv_file');
        $csvFile->move(base_path('\store\app\uploads'), $csvFile->getClientOriginalName());

        ProcessCsv::dispatch(base_path('\store\app\uploads') . '\\' . $csvFile->getClientOriginalName());
    }
}
