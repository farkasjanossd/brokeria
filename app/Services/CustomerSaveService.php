<?php

namespace App\Services;

use App\Models\Customer;
use App\Requests\CustomersUploadRequest;
use Carbon\Carbon;

readonly class CustomerSaveService
{
    /**
     * @param string $path
     */
    public function __construct(
        private readonly string $path
    ) {}

    /**
     * Save customers function.
     *
     * @return void
     */
    public function save(): void
    {
        $csvData = file_get_contents($this->path);
        $parsedData = str_getcsv($csvData, "\n");

        array_shift($parsedData);
        $chunkedData = array_chunk($parsedData, 1000);
        foreach ($chunkedData as $chunk) {
            $this->saveChunk($chunk);
        }
    }

    /**
     * Save chunks to db.
     *
     * @param array $chunk
     *
     * @return void
     */
    private function saveChunk(array $chunk): void
    {
        $dataToInsert = array_map(function ($row) {
            $rowData = str_getcsv($row);
            if (trim($row) !== '') {
                return [
                    'user_name' => $rowData[0],
                    'email' => $rowData[1],
                    'creditCard_issuer' => $rowData[2],
                    'iban' => $rowData[3],
                    'ipv4' => $rowData[4],
                    'birth_date' => Carbon::parse(str_replace('"', '', $rowData[5])),
                ];
            }
        }, $chunk);
        Customer::query()->insert($dataToInsert);
    }
}
