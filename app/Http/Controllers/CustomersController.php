<?php

namespace App\Http\Controllers;

use App\Jobs\ProcessCsv;
use App\Requests\CustomersUploadRequest;
use App\Resources\CustomerResource;
use App\Services\CustomerService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CustomersController extends Controller
{

    /**
     * Get paginated customer data.
     *
     * @param Request $request
     *
     * @return AnonymousResourceCollection
     */
    public function getCustomersData(Request $request): AnonymousResourceCollection
    {
        return CustomerResource::collection(CustomerService::getPaginatedCustomerData($request));
    }

    /**
     * File uploader function.
     *
     * @param CustomersUploadRequest $request
     *
     * @return JsonResponse
     */
    public function uploadCustomersData(CustomersUploadRequest $request): JsonResponse
    {

        try {
            CustomerService::uploadAndProcessFile($request);
            return new JsonResponse(['message' => 'Job added to queue']);
        } catch (Exception $e) {

            return new JsonResponse(['message' => $e->getMessage()]);
        }
    }
}
