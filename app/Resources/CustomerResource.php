<?php

namespace App\Resources;

use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin  Customer */
class CustomerResource extends JsonResource
{

    /**
     * Convert model data into array.
     *
     * @param Request $request
     *
     * @return array
     */
    public function toArray(Request $request)
    {
        return [
            'id' => $this->id,
            'user_name' => $this->user_name,
            'email' => $this->email,
            'creditCard_issuer' => $this->credit_card_issuer,
            'iban' => $this->iban,
            'ipv4' => $this->ipv4,
            'birth_date' => $this->birth_date,
        ];
    }
}
