<?php

namespace App\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomersUploadRequest extends FormRequest
{
    /**
     * Indicates whether validation should stop after the first rule failure.
     *
     * @var bool
     */
    public $stopOnFirstFailure = true;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'csv_file' => [
                'required',
                'file',
                'mimes:csv,txt',
            ]
        ];
    }

    /**
     * Error messages.
     *
     * @return array<string,string>
     */
    public function messages(): array
    {
        return [
            'csv_file.required' => 'File not selected.',
            'csv_file.file' => 'Please upload a file.',
            'csv_file.mimes' => 'Uplodad file mime is not :mimes.'
        ];
    }
}
