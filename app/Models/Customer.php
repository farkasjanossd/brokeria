<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $user_name
 * @property string $email
 * @property string $credit_card_issuer
 * @property string $iban
 * @property string $ipv4
 * @property Carbon $birth_date
 *
 */
class Customer extends Model
{
    /**
     * Fillable attributes.
     *
     * @var array<int,string>
     */
    protected $fillable = [
        'user_name',
        'email',
        'credit_card_issuer',
        'iban',
        'ipv4',
        'birth_date',
    ];
}
