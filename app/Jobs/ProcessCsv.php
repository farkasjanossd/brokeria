<?php

namespace App\Jobs;

use App\Services\CustomerSaveService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessCsv implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct(private readonly string $path)
    {}

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        (new CustomerSaveService($this->path))->save();
    }
}
