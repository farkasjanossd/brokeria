import {createApp} from "vue";
import App from "./App.vue";
import router from "./router";

import {ServerTable} from 'v-tables-3';
// Import Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';
const app = createApp(App);

app.use(router)
    .use(ServerTable);

router.isReady().then(() => {
    app.mount('#app');
});
