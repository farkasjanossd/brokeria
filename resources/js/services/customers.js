import API from './API';

export default {
    upload(data) {
        return API().post('/csv/upload', data);
    },
    getData(data) {
        return API().get('/csv/list', {params: data});
    }
}
