import axios from "axios";
import router from "../router/index.js";

export default (url = '/api') => {

    let client = axios.create({
        baseURL: url,
    });

    client.interceptors.response.use(
        (response) => {
            return response;
        },
        (error) => {
            if (error?.response?.status === 404) {
                console.log('404 not found...')
            } else {
                return Promise.reject(error)
            }
        }
    );
    return client;
}
