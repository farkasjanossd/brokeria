import {createRouter, createWebHistory} from 'vue-router';


const routes = [
    //dashboard
    {
        path: '/',
        name: 'list',
        component: () => import('../views/list.vue'),
    },
    {
        path: '/upload',
        name: 'upload',
        component: () => import('../views/upload.vue'),
    },
];

const router = new createRouter({
    mode: 'history',
    history: createWebHistory(),
    linkExactActiveClass: 'active',
    routes,
});

export default router;
