import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';
import vue from "@vitejs/plugin-vue";
import {VitePWA} from 'vite-plugin-pwa';

export default defineConfig(({mode}) => ({
    define: {
        __VUE_PROD_DEVTOOLS__: mode !== 'production',
        __VUE_PROD_HYDRATION_MISMATCH_DETAILS__: mode !== 'production'
    },
    plugins: [
        laravel({
            input: ['resources/css/app.css', 'resources/js/app.js'],
            refresh: true,
        }),
        vue({
            template: {
                transformAssetUrls: {
                    base: null,
                    includeAbsolute: false,
                },
            },
        }),
        VitePWA({
            registerType: 'autoUpdate',
            injectRegister: 'script',
            devOptions: {
                enabled: mode !== 'production',
            },
            workbox: {
                globPatterns: ['**/*.{js,css}'],
                navigateFallback: null,
                cleanupOutdatedCaches: true,
            }
        }),
    ],
    resolve: {
        dedupe: [
            'vue'
        ],
        alias: [
            {
                find: /^~(.*)$/,
                replacement: "node_modules/$1",
            },
        ],
    },
}));
